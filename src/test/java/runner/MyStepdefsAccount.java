package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.*;
import session.Session;

import java.util.Map;
import java.util.Random;

public class MyStepdefsAccount {
    MainPage mainPage = new MainPage();
    SignupModal signupModal = new SignupModal();
    LoginModal loginModal = new LoginModal();
    SettingsModal settingsModal = new SettingsModal();
    MenuSection menuSection= new MenuSection();
    Random random = new Random();
    Integer randomValue = random.nextInt(5000 + 1000) + 1000;

    @Given("the page {string} is opened")
    public void thePageIsOpened(String url) {
        Session.getInstance().getBrowser().get(url);
    }

    @And("I want to create and account")
    public void iWantToCreateAndAccount(Map<String, String> accountData) {
        mainPage.signupButton.click();
        signupModal.fullNameTxtBox.writeText(accountData.get("fullname"));
        signupModal.emailTxtBox.writeText(randomValue.toString() + accountData.get("email"));
        signupModal.passwordTxtBox.writeText(accountData.get("password"));
        signupModal.readCheckBox.click();
        signupModal.signupButton.click();
    }
    @Then("I should see the website")
    public void iShouldSeeTheWebsite() {
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(), "The create account was failed");
    }

    @And("Press the settings option for change the password")
    public void pressTheSettingsOptionForChangeThePassword() {
        menuSection.settingsButton.click();
    }

    @When("Set the current password {string} and new password {string}")
    public void setTheCurrentPasswordAndNewPassword(String oldPassword, String newPassword) {
        settingsModal.oldPasswordTxtBox.writeText(oldPassword);
        settingsModal.newPasswordTxtBox.writeText(newPassword);
    }

    @And("Save the changes in account")
    public void saveTheChangesInAccount() {
        settingsModal.saveButton.click();
    }

    @Then("I should see the website again")
    public void iShouldSeeTheWebsiteAgain() {
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(), "ERROR: The create account was failed");
        menuSection.logoutButton.click();
    }

    @And("I want to login into website")
    public void iWantToLoginIntoWebsite(Map<String, String> credentials) {
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(randomValue.toString() + credentials.get("email"));
        loginModal.pwdTxtBox.writeText(credentials.get("password"));
        loginModal.loginButton.click();
    }

    @Then("I should sing into the website")
    public void iShouldSingIntoTheWebsite() {
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(), "ERROR: The login into account was fail");
    }
}
