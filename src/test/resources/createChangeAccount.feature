Feature: Account

  Scenario: Create an account and change the current password

    Given the page "http://todo.ly/" is opened
    And I want to create and account
      |fullname |Full Name Account |
      |email    |mail@email.com   |
      |password |1234567           |
    Then I should see the website
    And Press the settings option for change the password
    When Set the current password "1234567" and new password "7654321"
    And Save the changes in account
    Then I should see the website again

    Given the page "http://todo.ly/" is opened
    And I want to login into website
      | email    | mail@email.com |
      | password | 7654321         |
    Then I should sing into the website
